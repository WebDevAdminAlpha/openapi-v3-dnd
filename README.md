# OpenAPI 

## Documentation
- [OpenAPI Specification - Version 3.0.0 - Documentation](https://swagger.io/docs/specification/basic-structure/)

## Schemas
- [OpenAPI Specification - Version 3.0.0 - Schema](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md)
- [OpenAPI Specification - Version 3.0.1 - Schema](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.1.md)
- [OpenAPI Specification - Version 3.0.2 - Schema](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
- [OpenAPI Specification - Version 3.0.3 - Schema](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md)